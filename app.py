import os

import numpy as np
from flask import Flask, request, render_template, redirect, url_for
import pandas as pd
import plotly.graph_objs as go
import os

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        file = request.files.get('file')
        if file and file.filename.endswith('.xlsx'):
            save_path = f'files/{file.filename}'
            file.save(save_path)
            return redirect(url_for('heatmap', filename=save_path))
    file_names = [f for f in os.listdir('files/') if os.path.isfile(os.path.join('files/', f))]
    return render_template('index.html', filenames=file_names)


@app.route('/scatter')
def scatter():
    filename = request.args.get('filename', None)
    if not filename or not os.path.exists(filename):
        return redirect(url_for('index'))

    data = pd.read_excel(filename, header=None)
    if data.shape[1] < 3:
        return "The file must have at least three columns."

    data.columns = ['Field1', 'Field2', 'Value']
    num_records = len(data)

    # Расчет координат
    num_columns = int(np.sqrt(num_records) * 1.307692)
    num_rows = int(np.sqrt(num_records) * 0.764706)
    while num_rows * num_columns < num_records:
        num_rows += 1

    positions = []
    sizes = []
    colors = []
    texts = []

    # Настройка данных для кругов
    for i in range(len(data)):
        col = i % num_columns
        row = i // num_columns
        positions.append((col, row))
        sizes.append(20)  # Размер круга (можно адаптировать)
        colors.append(data.at[i, 'Value'])
        texts.append(f"{data.at[i, 'Field1']}, {data.at[i, 'Field2']}")

    scatter_data = go.Scatter(
        x=[pos[0] for pos in positions],
        y=[pos[1] for pos in positions],
        mode='markers',
        marker=dict(
            size=sizes,
            color=colors,
            colorscale='RdYlGn',
            showscale=False
        ),
        text=texts,
        hoverinfo='text'
    )

    layout = go.Layout(
        xaxis=dict(showticklabels=False, showgrid=False, zeroline=False, showline=False),
        yaxis=dict(showticklabels=False, showgrid=False, zeroline=False, showline=False),
        showlegend=False,
        autosize=True,
        margin=dict(l=0, r=0, t=0, b=0),
        paper_bgcolor='black',  # Устанавливаем цвет фона документа
        plot_bgcolor='black',

    )

    fig = go.Figure(data=[scatter_data], layout=layout)
    heatmap_html = fig.to_html(include_plotlyjs='cdn', full_html=False, config={
        'displayModeBar': True,
        'modeBarButtonsToRemove': ['zoomOut2d']
    })

    return f"""
        <html>
            <head>
                <title>Scatter Visualization</title>
                <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
            </head>
            <body>
                <div style="background-color: black; align-items: center; justify-content: center; display: flex; padding-top: 20px;">
                    <button style="margin: 10px;" onclick="window.location.href='/'" class="btn btn-primary">Back to upload</button>
                    <button style="margin: 10px;" onclick="window.location.href='/heatmap?filename={filename}'" class="btn btn-secondary">Heatmap</button>
                    <button onclick="window.location.href='/heatmap2?filename={filename}'" class="btn btn-secondary">Heatmap #2</button>

                </div>
                {heatmap_html}
            </body>
        </html>
    """


@app.route('/heatmap')
def heatmap():
    filename = request.args.get('filename', None)
    if not filename or not os.path.exists(filename):
        return redirect(url_for('index'))

    data = pd.read_excel(filename, header=None)
    if data.shape[1] < 3:
        return "The file must have at least three columns."

    data.columns = ['Field1', 'Field2', 'Value']
    num_records = len(data)

    num_columns = int(np.sqrt(num_records) * 1.307692)
    num_rows = int(np.sqrt(num_records) * 0.764706)
    while num_rows * num_columns < num_records:
        num_rows += 1

    reshaped_values = np.full((num_rows, num_columns), np.nan)
    reshaped_text = np.full((num_rows, num_columns), '', dtype=object)

    for i in range(len(data)):
        col = i % num_columns
        row = i // num_columns
        reshaped_values[row, col] = data.at[i, 'Value']
        reshaped_text[row, col] = f"{data.at[i, 'Field1']}, {data.at[i, 'Field2']}"

    heatmap_data = go.Heatmap(
        z=reshaped_values,
        x=np.arange(num_columns),
        y=np.arange(num_rows),
        text=reshaped_text,
        hoverinfo='text',
        colorscale='RdYlGn',
        showscale=False
    )

    layout = go.Layout(
        xaxis=dict(showticklabels=False, showgrid=False),
        yaxis=dict(showticklabels=False, showgrid=False),
        showlegend=False,
        autosize=True,
        margin=dict(l=0, r=0, t=0, b=0)
    )

    fig = go.Figure(data=[heatmap_data], layout=layout)
    heatmap_html = fig.to_html(include_plotlyjs='cdn', full_html=False, config={
        'displayModeBar': True,  # Оставляем панель инструментов видимой
        'modeBarButtonsToRemove': ['zoomOut2d']  # Убираем кнопки масштабирования и авто-масштабирования
    })

    return f'''
        <html>
            <head>
                <title>Heatmap Visualization #1</title>
                <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
            </head>
            <body>
                <div style="background-color: black; align-items: center; justify-content: center; display: flex; padding-top: 20px;">
                    <button style="margin: 10px;" onclick="window.location.href='/'" class="btn btn-primary">Back to upload</button>
                    <button style="margin: 10px;" onclick="window.location.href='/heatmap2?filename={filename}'" class="btn btn-secondary">Heatmap #2</button>

                    <button onclick="window.location.href='/scatter?filename={filename}'" class="btn btn-secondary">Scatter</button>
                    
                </div>
                {heatmap_html}
            </body>
        </html>
    '''

@app.route('/heatmap2')
def heatmap2():
    filename = request.args.get('filename', None)
    if not filename or not os.path.exists(filename):
        return redirect(url_for('index'))

    data = pd.read_excel(filename, header=None)
    if data.shape[1] < 3:
        return "The file must have at least three columns."

    data.columns = ['Field1', 'Field2', 'Value']
    num_records = len(data)

    num_columns = int(np.sqrt(num_records) * 1.307692)
    num_rows = int(np.sqrt(num_records) * 0.764706)
    while num_rows * num_columns < num_records:
        num_rows += 1

    reshaped_values = np.full((num_rows, num_columns), np.nan)
    reshaped_text = np.full((num_rows, num_columns), '', dtype=object)

    for i in range(len(data)):
        col = i % num_columns
        row = i // num_columns
        reshaped_values[row, col] = data.at[i, 'Value']
        reshaped_text[row, col] = f"{data.at[i, 'Field1']}, {data.at[i, 'Field2']}"

    heatmap_data = go.Heatmap(
        z=reshaped_values,
        x=np.arange(num_columns),
        y=np.arange(num_rows),
        text=reshaped_text,
        hoverinfo='text',
        colorscale='RdYlGn',
        showscale=False
    )

    # Создаем сетку с учетом позиций
    shapes = []
    for i in range(num_rows + 1):
        shapes.append({
            'type': 'line',
            'x0': -0.5,
            'y0': i - 0.5,
            'x1': num_columns - 0.5,
            'y1': i - 0.5,
            'line': {
                'color': 'black',
                'width': 12,
            },
        })
    for i in range(num_columns + 1):
        shapes.append({
            'type': 'line',
            'x0': i - 0.5,
            'y0': -0.5,
            'x1': i - 0.5,
            'y1': num_rows - 0.5,
            'line': {
                'color': 'black',
                'width': 12
            },
        })

    layout = go.Layout(
        xaxis=dict(showticklabels=False, showgrid=False, range=[-0.5, num_columns - 0.5]),
        yaxis=dict(showticklabels=False, showgrid=False, range=[-0.5, num_rows - 0.5]),
        shapes=shapes,
        showlegend=False,
        autosize=True,
        margin=dict(l=0, r=0, t=0, b=0),
    )

    fig = go.Figure(data=[heatmap_data], layout=layout)
    heatmap_html = fig.to_html(include_plotlyjs='cdn', full_html=False, config={
        'displayModeBar': True,
        'modeBarButtonsToRemove': ['zoomOut2d']
    })

    return f'''
        <html>
            <head>
                <title>Heatmap Visualization #2</title>
                <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
            </head>
            <body>
                <div style="background-color: black; align-items: center; justify-content: center; display: flex; padding-top: 20px;">
                    <button style="margin: 10px;" onclick="window.location.href='/'" class="btn btn-primary">Back to upload</button>
                    <button style="margin: 10px;" onclick="window.location.href='/heatmap?filename={filename}'" class="btn btn-secondary">Heatmap</button>
                    <button onclick="window.location.href='/scatter?filename={filename}'" class="btn btn-secondary">Scatter</button>
                </div>
                {heatmap_html}
            </body>
        </html>
    '''


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8091, debug=True)
